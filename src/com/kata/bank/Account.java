package com.kata.bank;

import com.kata.bank.Enums.OperationType;

public class Account {

    private static final String NO_AMOUNT_ERROR = "No amount to add!";
    private static final String NEGATIVE_AMOUNT_ERROR = "Please enter a positive number!";

    private Client client;
    private Statement statement;

    public Account(Client client){
        this.client = client;
        this.statement = new Statement();
    }

    public Client getClient() {
        return client;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void addOperation(OperationType operationType, double amount){
        try {
             if (amount == 0) throw new Exception(NO_AMOUNT_ERROR);
             else if(amount<0) throw new Exception(NEGATIVE_AMOUNT_ERROR);
             else statement.addOperation(operationType, amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showHistory(){
        client.show();
        statement.showHistory();
    }
}
