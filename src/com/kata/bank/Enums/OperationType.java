package com.kata.bank.Enums;

public enum OperationType {

    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw");

    private String operationType;

    public String getValue(){return operationType;}

    OperationType(String operationType){
        this.operationType = operationType;
    }
}
