package com.kata.bank.main;

import com.kata.bank.Account;
import com.kata.bank.Client;
import com.kata.bank.Enums.OperationType;
import com.kata.bank.Operation;

public class Main {

    public static void main(String[] args) {
        Client client = new Client("wassim", "hj"," whj@domain.com");
        Account acc = new Account(client);
        acc.addOperation(OperationType.WITHDRAW, -100);
        acc.addOperation(OperationType.DEPOSIT, 100);
        acc.addOperation(OperationType.DEPOSIT, 200.5);
        acc.addOperation(OperationType.DEPOSIT, 0);
        acc.addOperation(OperationType.WITHDRAW, 300);
        acc.addOperation(OperationType.WITHDRAW, 1);
        acc.showHistory();
    }
}
