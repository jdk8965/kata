package com.kata.bank;

import com.kata.bank.Enums.OperationType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Statement {

    private static final String BALANCE_MESSAGE = "Account Balance: %.3f";
    private static final String OPERATIONS_MESSAGE = "-Operations";
    private static final String WITHDRAW_ERROR = "Could not withdraw %.3f, Amount below balance!";
    private static final int MIN_WITHDRAW_AMOUNT = 20;
    private static final String MIN_WITHDRAW_AMOUNT_ERROR = "Impossible to withdraw %.3f, amount below minimum 20!";

    private BigDecimal balance;
    private List<Operation> operations;

    public Statement(){
        this.balance = new BigDecimal(0.0);
        operations = new ArrayList<>();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void addOperation(OperationType operationType, double amount){
        try{
            if (operationType.equals(OperationType.DEPOSIT))
                deposit(amount);
            else if (operationType.equals(OperationType.WITHDRAW))
                withdraw(amount);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deposit(double amount){
        Operation op = new Operation();
        op.setAmount(amount);
        BigDecimal newBalance = balance.add(BigDecimal.valueOf(amount));
        op.setBalance(newBalance);
        op.setOperationType(OperationType.DEPOSIT.getValue());
        operations.add(op);
        balance=newBalance;
    }

    public void withdraw(double amount){
        try{
            BigDecimal amountBg = BigDecimal.valueOf(amount);
            if(amountBg.compareTo(BigDecimal.valueOf(MIN_WITHDRAW_AMOUNT))==-1)
                throw new Exception(String.format(MIN_WITHDRAW_AMOUNT_ERROR, amount));
            if(amountBg.compareTo(balance)==1)
                throw new Exception(String.format(WITHDRAW_ERROR, amount));
            Operation op = new Operation();
            op.setAmount(amount);
            BigDecimal newBalance = balance.subtract(amountBg);
            op.setBalance(newBalance);
            op.setOperationType(OperationType.WITHDRAW.getValue());
            operations.add(op);
            balance=newBalance;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showHistory(){
        System.out.println(String.format(BALANCE_MESSAGE, balance));
        System.out.println(OPERATIONS_MESSAGE);
        for(Operation op : operations)
            op.show();
    }
}
