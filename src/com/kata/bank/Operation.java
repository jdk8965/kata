package com.kata.bank;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Operation {

    private static final String OPERATION_MESSAGE = "DATE: %s, AMOUNT: %.3f, TYPE:%s, BALANCE: %.3f";
    private static final String DATE_FROMAT = "YYYY/MM/dd:hh:mm:ss";

    private Date date;
    private double amount;
    private BigDecimal balance; // post operation account balance
    private String operationType;

    public Operation(){
        this.date = new Date();
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public void show(){
        String formatedDate = new SimpleDateFormat(DATE_FROMAT).format(date);
        System.out.println(String.format(OPERATION_MESSAGE, formatedDate, amount, operationType, balance));
    }
}
