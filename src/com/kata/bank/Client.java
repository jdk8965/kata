package com.kata.bank;

public class Client {

    private static final String CLIENT_MESSAGE = "Firstname: %s, Lastname: %s, email: %s";

    private String firstName;
    private String lastName;
    private String email;

    public Client(String firstName, String lastName, String email){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void show() {
        System.out.println(String.format(CLIENT_MESSAGE, firstName, lastName, email));
    }
}
