package com.kata.algo;

public class FooBarQix {

    public static final String FOO = "Foo";
    public static final String BAR = "Bar";
    public static final String QIX = "Qix";

    public static String getFooBarQix(int n) {
        StringBuilder s = new StringBuilder();
        for (int i = 1; i <= n; i++) {
            StringBuilder str = new StringBuilder();
            if (i % 3 == 0)  str.append(FOO);
            if (i % 5 == 0) str.append(BAR);
            for (char c : String.valueOf(i).toCharArray()) {
                if(c=='3') str.append(FOO);
                else if(c=='5') str.append(BAR);
                else if(c=='7') str.append(QIX);
            }
            if (!String.valueOf(str).isEmpty()) s.append(str);
            else s.append(i);
            s.append(" ");
        }
        return String.valueOf(s);
    }

    public static void main(String[] args) {
        int n = 100;
        System.out.println(getFooBarQix(n));
        }
    }

